﻿namespace Experity_Coding_Challenge
{
    partial class CodingChallenge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialogButton = new System.Windows.Forms.Button();
            this.wordAnalysisTextbox = new System.Windows.Forms.TextBox();
            this.StartButton = new System.Windows.Forms.Button();
            this.fileLocationLabel = new System.Windows.Forms.Label();
            this.ClearButton = new System.Windows.Forms.Button();
            this.HelpingLabel = new System.Windows.Forms.Label();
            this.wordGrid = new System.Windows.Forms.DataGridView();
            this.percentLabel = new System.Windows.Forms.Label();
            this.numberOfSelectedWordLabel = new System.Windows.Forms.Label();
            this.numberOfWordsLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.wordGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialogButton
            // 
            this.openFileDialogButton.Location = new System.Drawing.Point(63, 36);
            this.openFileDialogButton.Name = "openFileDialogButton";
            this.openFileDialogButton.Size = new System.Drawing.Size(91, 23);
            this.openFileDialogButton.TabIndex = 0;
            this.openFileDialogButton.Text = "Open File";
            this.openFileDialogButton.UseVisualStyleBackColor = true;
            this.openFileDialogButton.Click += new System.EventHandler(this.OpenFileDialogButton_Click);
            // 
            // wordAnalysisTextbox
            // 
            this.wordAnalysisTextbox.Location = new System.Drawing.Point(194, 79);
            this.wordAnalysisTextbox.Name = "wordAnalysisTextbox";
            this.wordAnalysisTextbox.Size = new System.Drawing.Size(322, 20);
            this.wordAnalysisTextbox.TabIndex = 1;
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(541, 36);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(75, 23);
            this.StartButton.TabIndex = 3;
            this.StartButton.Text = "Start";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // fileLocationLabel
            // 
            this.fileLocationLabel.AutoSize = true;
            this.fileLocationLabel.Location = new System.Drawing.Point(191, 41);
            this.fileLocationLabel.Name = "fileLocationLabel";
            this.fileLocationLabel.Size = new System.Drawing.Size(0, 13);
            this.fileLocationLabel.TabIndex = 4;
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(541, 77);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 5;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // HelpingLabel
            // 
            this.HelpingLabel.AutoSize = true;
            this.HelpingLabel.Location = new System.Drawing.Point(60, 82);
            this.HelpingLabel.Name = "HelpingLabel";
            this.HelpingLabel.Size = new System.Drawing.Size(70, 13);
            this.HelpingLabel.TabIndex = 6;
            this.HelpingLabel.Text = "Enter a word:";
            // 
            // wordGrid
            // 
            this.wordGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.wordGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.wordGrid.Location = new System.Drawing.Point(63, 118);
            this.wordGrid.Name = "wordGrid";
            this.wordGrid.ReadOnly = true;
            this.wordGrid.Size = new System.Drawing.Size(553, 240);
            this.wordGrid.TabIndex = 7;
            // 
            // percentLabel
            // 
            this.percentLabel.AutoSize = true;
            this.percentLabel.Location = new System.Drawing.Point(60, 407);
            this.percentLabel.Name = "percentLabel";
            this.percentLabel.Size = new System.Drawing.Size(253, 13);
            this.percentLabel.TabIndex = 8;
            this.percentLabel.Text = "Percentage of selected word vs overall word count: ";
            // 
            // numberOfSelectedWordLabel
            // 
            this.numberOfSelectedWordLabel.AutoSize = true;
            this.numberOfSelectedWordLabel.Location = new System.Drawing.Point(60, 361);
            this.numberOfSelectedWordLabel.Name = "numberOfSelectedWordLabel";
            this.numberOfSelectedWordLabel.Size = new System.Drawing.Size(223, 13);
            this.numberOfSelectedWordLabel.TabIndex = 9;
            this.numberOfSelectedWordLabel.Text = "Number of occurrences of the selected word: ";
            // 
            // numberOfWordsLabel
            // 
            this.numberOfWordsLabel.AutoSize = true;
            this.numberOfWordsLabel.Location = new System.Drawing.Point(60, 384);
            this.numberOfWordsLabel.Name = "numberOfWordsLabel";
            this.numberOfWordsLabel.Size = new System.Drawing.Size(139, 13);
            this.numberOfWordsLabel.TabIndex = 10;
            this.numberOfWordsLabel.Text = "Number of words in this file: ";
            // 
            // CodingChallenge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 439);
            this.Controls.Add(this.numberOfWordsLabel);
            this.Controls.Add(this.numberOfSelectedWordLabel);
            this.Controls.Add(this.percentLabel);
            this.Controls.Add(this.wordGrid);
            this.Controls.Add(this.HelpingLabel);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.fileLocationLabel);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.wordAnalysisTextbox);
            this.Controls.Add(this.openFileDialogButton);
            this.MaximumSize = new System.Drawing.Size(698, 478);
            this.MinimumSize = new System.Drawing.Size(698, 478);
            this.Name = "CodingChallenge";
            this.Text = "Experity Coding Challenge";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wordGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button openFileDialogButton;
        private System.Windows.Forms.TextBox wordAnalysisTextbox;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Label fileLocationLabel;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Label HelpingLabel;
        private System.Windows.Forms.DataGridView wordGrid;
        private System.Windows.Forms.Label percentLabel;
        private System.Windows.Forms.Label numberOfSelectedWordLabel;
        private System.Windows.Forms.Label numberOfWordsLabel;
    }
}

