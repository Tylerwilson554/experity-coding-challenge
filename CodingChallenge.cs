﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;

namespace Experity_Coding_Challenge
{
    public partial class CodingChallenge : Form
    {
        string[] contentArray;
        List<string> lineList = new List<string>();
        string selectedWord;
        bool isFileOpen = false;
        double selectedWordCount;
        double overallWordCount;
        int lineNumber;
        public CodingChallenge()
        {
            InitializeComponent();
        }
        //-----------------------------------------------------------------------------------------
        // Runs when the form is loaded. Creates the grid and runs InitializeVariables.
        //-----------------------------------------------------------------------------------------
        private void Form1_Load(object sender, EventArgs e)
        {
            wordGrid.Columns.Add("selectedWord", "Selected Word");           
            wordGrid.Columns.Add("previousWord", "Previous Word");
            wordGrid.Columns.Add("nextWord", "Next Word");
            wordGrid.Columns.Add("lineNumber", "Line Number");
            InitializeVariables();
        }
        //-----------------------------------------------------------------------------------------
        // Runs when the Open File button is clicked. It opens the file and then adds each
        //  line into a list. It then changes a label next to the button to show the file name. 
        //-----------------------------------------------------------------------------------------
        private void OpenFileDialogButton_Click(object sender, EventArgs e)
        {
            lineList.Clear();
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    var filePath = openFileDialog.FileName;
                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();
                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        while (!reader.EndOfStream)
                        {
                            lineList.Add(reader.ReadLine());
                        }                    
                    }
                    fileLocationLabel.Text = "Analyzing: " + filePath;
                    isFileOpen = true;
                }
                else
                {
                    isFileOpen = false;
                    fileLocationLabel.Text = string.Empty;
                }
            }     
        }
        //-----------------------------------------------------------------------------------------
        // Sets most global variables and various labels on the interface back to default.
        // This also will clear the grid.
        //-----------------------------------------------------------------------------------------
        private void InitializeVariables()
        {
            contentArray = Array.Empty<string>();
            selectedWord = string.Empty;
            numberOfSelectedWordLabel.Text = "Number of occurrences of the selected word: ";
            numberOfWordsLabel.Text = "Number of words in this file: ";
            percentLabel.Text = "Percentage of selected word vs overall word count: ";
            HelpingLabel.Text = "Enter a word: ";
            HelpingLabel.ForeColor = Color.Black;
            wordGrid.Rows.Clear();
            selectedWordCount = 0;
            overallWordCount = 0;
            lineNumber = 0;      
        }
        //-----------------------------------------------------------------------------------------
        // Runs when the start button is clicked. This will check if a file is already open,
        //  and then check if the selected word is null or white space or empty.
        //  If the word is valid it will run InitializeVariables, set the selected word variable
        //  to the text that is in the textbox and then run AnalyzeFile
        //-----------------------------------------------------------------------------------------
        private void StartButton_Click(object sender, EventArgs e)
        {
            if (isFileOpen)
            {
                if (string.IsNullOrWhiteSpace(wordAnalysisTextbox.Text.Trim()))
                {
                    HelpingLabel.Text = "You must enter a word!";
                    HelpingLabel.ForeColor = Color.Red;
                    wordAnalysisTextbox.Focus();
                }
                else
                {
                    InitializeVariables();
                    selectedWord = wordAnalysisTextbox.Text;
                    AnalyzeFile();
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        // Runs when the clear button is clicked. This calls InitializeVariables, and empties
        //  the textbox.
        //-----------------------------------------------------------------------------------------
        private void ClearButton_Click(object sender, EventArgs e)
        {
            InitializeVariables();
            wordAnalysisTextbox.Text = string.Empty;
        }
        //-----------------------------------------------------------------------------------------
        // Steps through the list, line by line. The line number is taken, and then the line is 
        //  split into words and passed into an array. The array is then stepped through, word by
        //  by word. It will increment the overall word count for every word, and then check if 
        //  the word contains the selected word. If so, it will call OccurrenceFound using the
        //  incrementing variable and the line number. It then clears the line when it is done 
        //  with it, and calls WriteWordCounts when the end of the list has been reached.
        //-----------------------------------------------------------------------------------------
        public void AnalyzeFile()
        {
            foreach (string line in lineList)
            {               
                lineNumber = lineList.FindIndex(lineNumber, x => x == line) + 1;                
                contentArray = line.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < contentArray.Count(); i++)
                {
                    overallWordCount++;
                    if (contentArray[i].ToLowerInvariant().Contains(selectedWord.ToLowerInvariant()))
                    {
                        OccurrenceFound(i, lineNumber);
                    }
                }
                contentArray = Array.Empty<string>();
            }
            WriteWordCounts();       
        }
        //-----------------------------------------------------------------------------------------
        // Adds a row to the grid with the selected word that was found, the previous word, the 
        //  next word, and the line number that the word was found on. Also increments the 
        //  selected word count variable.
        //-----------------------------------------------------------------------------------------
        private void OccurrenceFound(int num, int lineNumber)
        {
            selectedWordCount++;
            wordGrid.Rows.Add(contentArray[num], GetPreviousWord(num), GetNextWord(num, contentArray.Count()), lineNumber);
            wordGrid.Update();
        }
        //-----------------------------------------------------------------------------------------
        // Checks if the selected word is the first word in the array, or if the previous word ends
        //  with a period, and returns "NA" if either is true. Returns the previous word if false.
        //-----------------------------------------------------------------------------------------
        private string GetPreviousWord(int counter)
        {
            if (counter == 0 || contentArray[counter - 1].EndsWith("."))
            {
                return "NA";
            }
            else
            {
                return contentArray[counter - 1];
            }
        }
        //-----------------------------------------------------------------------------------------
        // Checks if the selected word is the last word in the array, or if it ends with a period,
        //  and returns "NA" if either is true. It returns the next word in the array if false.
        //-----------------------------------------------------------------------------------------
        private string GetNextWord(int counter, int lengthOfArray)
        {
            if (counter == lengthOfArray - 1 || contentArray[counter].EndsWith("."))
            {
                return "NA";
            }
            else
            {
                return contentArray[counter + 1];
            }
        }
        //-----------------------------------------------------------------------------------------
        // Calculates the percentage of selected words vs the overall word count, and then 
        //  displays the the occurrences of the selected word, the overall words, and the 
        //  percentage. 
        //-----------------------------------------------------------------------------------------
        private void WriteWordCounts()
        {
            var percentage = 100 * (selectedWordCount / overallWordCount);
            numberOfSelectedWordLabel.Text = "Number of occurrences of the selected word: " + selectedWordCount;
            numberOfWordsLabel.Text = "Number of words in this file: " + overallWordCount;
            percentLabel.Text = "Percentage of selected word vs overall word count: " + percentage.ToString("n2") + "%";
        }
    }
}